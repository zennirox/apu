## the decisions table
performanceMatrix  <- rbind(
  c(5,1,16,8,999),
  c(5,1,8,8,799),
  c(5,2,32,20,1200),
  c(4.7,1,8,5,600),
  c(4.7,1,8,5,600),
  c(5,1,8,5,320),
  c(5.2,3,32,20,1320),
  c(4.5,2,32,41,1500),
  c(6,2,32,20.7,1500),
  c(4.7,1,8,6.7,1000)
)

# Vector containing names of alternatives
alternatives <- c(
  "lumia650",
  "lumia640",
  "lumia930",
  "lumia550",
  "lumia532",
  "lumia535",
  "lumia950",
  "lumia1020",
  "lumia1520",
  "lumia735")

# Vector containing names of criteria
criteria <- c(
  "wyswietlacz",
  "pamiec_ram",
  "pamiec_wbudowana",
  "aprat",
  "cena")

IndifferenceThresholds  <- c(5,2,16,8,700)
PreferenceThresholds  <- c(4.7,1,8,5,500)
VetoThresholds  <- c(6,3,32,20,1200)

minmaxcriteria <- c('max', 'max', 'max', 'max', 'min')

criteriaWeights <- c(0.25,0.45,0.10,0.12,0.08)

# Matrix containing the profiles.
profiles <- cbind(c(4,-6),c(1,8),c(8,32),c(4,50),c(200,2000))

#  vector defining profiles' names
profiles_names <-c("b1","b2")

Electre_tri(performanceMatrix,
            alternatives,
            profiles,
            profiles_names,
            criteria,
            minmaxcriteria,
            criteriaWeights,
            IndifferenceThresholds,
            PreferenceThresholds,
            VetoThresholds,
            lambda=NULL)
